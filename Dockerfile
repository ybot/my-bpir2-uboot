FROM debian:bullseye
RUN apt-get update && apt-get -y install git gcc-arm-linux-gnueabihf make u-boot-tools bc gcc libc6-dev libncurses5-dev ccache libssl-dev flex bison dpkg-dev rsync kmod cpio xxd
WORKDIR /data
CMD ["/data/build-kernel.sh"]

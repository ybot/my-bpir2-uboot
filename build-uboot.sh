#!/bin/bash
set -Eeuo pipefail

: ${MY_UBOOT_BRANCH:=2020-10-bpi}
: ${MY_UBOOT_ENV:=${PWD}/configs/uEnv_r2.txt}
: ${MY_UBOOT_CONFIG:=${PWD}/configs/u-boot-2020-10.config}

: ${MY_BUILDDIR:="/tmp/my-bpir2-uboot-build"}
: ${MY_RESULT_DIR:="${PWD}"}

[ ! -d "${MY_BUILDDIR}" ] && mkdir -p "${MY_BUILDDIR}"
cd "${MY_BUILDDIR}"
[ ! -d "u-boot" ] && git clone -b ${MY_UBOOT_BRANCH} https://github.com/frank-w/u-boot.git

cp ${MY_UBOOT_CONFIG} ./u-boot/.config
cp ${MY_UBOOT_ENV} ./u-boot/uEnv_r2.txt

cd u-boot
./build.sh build

# keeping this down here, I wanna keep track of the upstream repo
my_uboot_branch=$(git rev-parse --abbrev-ref HEAD)
my_uboot_hash=$(git rev-parse --short HEAD)

cp u-boot.bin ${MY_RESULT_DIR}/u-boot-${my_uboot_branch}-${my_uboot_hash}.bin
